package programming;

public class NumPattern9 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        int num = 1;
        char cha = 'A';
        for(int i=0;i<line;i++){
            for(int j = 0;j<star;j++){
                if(i%2==0){
                    System.out.print(num++);
                }else{
                    System.out.print(cha++);
                }
            }System.out.println();
        }
    }
}
