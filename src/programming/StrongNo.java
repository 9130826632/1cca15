package programming;

import java.util.Scanner;

public class StrongNo {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter your Number");
        int a =sc.nextInt();
        int temp=a;
        int sum=0;

        while(a!=0){
            int r=a%10;
            int fact=1;
            for(int i=1;i<=r;i++){
                fact*=i;
            }
            sum+=fact;
            a=a/10;
        }
        if(sum==temp){
            System.out.println("Number is Strong no ");
        }else
            System.out.println("Number is not Strong no");
    }
}
