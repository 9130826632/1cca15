package Abstraction;

public class LoanAccount implements Account{

   private double loanAmount;

    public LoanAccount(double loanAmount){
        this.loanAmount=loanAmount;
        System.out.println("Loan Account Created");
    }

    @Override
    public void deposit(double amt) {
        loanAmount-=amt;
        System.out.println(amt+"Rs Debited from loan account");
    }

    @Override
    public void withdraw(double amt) {
        loanAmount+=amt;
        System.out.println(amt+"Rs Credited to your account");
    }

    @Override
    public void checkBalance() {
        System.out.println("Active Loan Amount"+loanAmount);
    }
}
