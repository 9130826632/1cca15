package Abstraction;

public class SavingsAccount implements Account{

   private double accountBalance;

    public SavingsAccount(double accountBalance){
        this.accountBalance=accountBalance;
        System.out.println("Savings Account Created");
    }

    @Override
    public void deposit(double amt) {
        accountBalance+=amt;
        System.out.println(amt+"Rs Credited to your account");
    }

    @Override
    public void withdraw(double amt) {
        if(amt<=accountBalance){
            accountBalance-=amt;
            System.out.println(amt+"Rs Debited from your account");
        }else{
            System.out.println("Insufficient Funds");
        }
    }

    @Override
    public void checkBalance() {
        System.out.println("Active Balance"+accountBalance);
    }
}
