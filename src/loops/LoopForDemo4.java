package loops;
import java.util.Scanner;
public class LoopForDemo4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Start Point");
        int start = sc.nextInt();

        System.out.println("Ener last Point");
        int end = sc.nextInt();

        int sum = 0;
        for (int a = start; a <= end; a++) {
            if (a%2==0) {
                sum += a;
            }
            System.out.println("Sum" + sum);
        }
    }
}
