package loops;
import java.util.Scanner;
public class ForLoopDemo3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter start Point");
        int start = sc.nextInt();

        System.out.println("Enter Last Point");
        int end = sc.nextInt();

        for(int a=start ;a<=end; a++)
            if(a%2==0)
            {
                System.out.println(a*a+ "");
            }
    }
}
