package loops;

import java.util.Scanner;

public class WhileLoopDemo2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int no = 1,sum=2;
        while(sum<=50 && no>=0){
            System.out.println("Enter no");
            no = sc.nextInt();
            sum= sum+no;
        }
            System.out.println(sum);
    }
}
